# EPCC Commerce Manager Terminology and Best Practices


## Concept Terms and Use-Case Best Practices

Here are a few key concepts and operational best practices that will help you to understand how to work within the Elastic Path Commerce Cloud (EPCC) Commerce Manager Dashboard.

## General Terms

#### **SLUG**

A slug is a value used in development as a reference to an item. Slug names must be unique, use lower case and underscore instead of spaces.

#### **Status**

Status allows you to post an item live to the front-end or save it as a draft. Once you set the status to live it is visible to all visitors. It is best practice to work in draft mode until you are ready to post your work.

#### **Description**

other than the product description. The description field is and internal reference. It is not visible on the front-end. It is best practice to get in the habit of including a detailed description for all entries. If you are working with other team members document your work helps everyone knows what entries are being used for.

#### **Image File Format**

The following are the accepted Commerce Manager upload file type formats:jpg, jpeg, png and gif

#### **Field types**

-	**Strings:** An finite sequence of characters (i.e., letters, numerals, symbols and punctuation marks). The length can be any natural number (i.e., zero or any positive integer). A string can be empty(empty string)that can contain no characters
-	**Booleans:**  A data type that can only have one of two values, like (YES / NO, ON / OFF or TRUE / FALSE)
-	**Dates:** A valid date values as strings (YYYY-MM-DD HH:MM:SS - time is optional)
-	**Integers:** An integer is a number that can be written without a fractional component. For example, 11, 2, 0, and −20 are integers
-	**Relationships:**  a connection between one or more resources


#### **Variation**
 
A variation are options also known as “choices” that a product has, in our t-shirt example this would be such as color or size.

#### **Modifier**

A modifier augments the properties of a base product, such as price, SKU, etc

#### **Stock Actions**
Stock can be adjusted using the following Actions
-	**Increment:** You’ll use this when you receive stock from a supplier, making products available for purchase
- **Decrement:** You’ll use this when you want to remove stock from product inventory
- **Allocate:** You’ll use this when you want to allocate stock, normally to a reseller who will sell on the stock
- **Deallocate:** You’ll use this when you want to deallocate any previously allocated stock



## Developer Terms

Here are some concepts and definitions that relate to the development or customization of the EPPC Commerce Manager.

#### **API**

API stands for "Application Program Interface"

#### Flows:
Flows allow you to extend an existing or create a new resource(such as products, customers and orders)by adding custom data fields and entries. Using Flows, you are able to create various types of fields, including strings, booleans, dates, integers and relationships with other custom flows. Three main entity types to understand when considering flow are: 
- **Flows:** a collection of fields
- **Fields** definitions of a piece of data to be collected within the Flow
- **Entries** the pieces of data collected within the fields.

#### **Webhooks**

A webhook is an API delivery concept. It enable data updates to be sent to an external public URL endpoints immediately. For example: A discount code or order confirmation email sent after an order has been fulfilled.  

#### **Integrations**

Is A feature in Commerce Manager that allows you to observe events and attach webhooks and connect to services outside of the EPCC ecosystem.

#### **Postman**
Postman is a collaboration platform for API development.
