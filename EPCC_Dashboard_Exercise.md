## Hands-on Exercises
---
## **Product Catalog and Files**

Practice what you've learned so far in the dashboard by creating a product catalog and building product items for the Playtend Games.

### Exercise 1: Creating a Catalog  

    1. Create three new categories
    2. Create a brand label for a new brand
    3. Created a collection to bundle or group different products from different categories together

### Exercise 2: Creating a Single Product (Parent Product)

    1. Create three physical product items
    2. Create a digital product item
    3. Extend the products to include inventory details
    4. Assign the products to a category, brand, and collection
    5. Add a main image

### Exercise 3: Build a Parent Product with Child Products using Variants

Building on a (Parent) product you've created, create a child product using a variant:
    1. Create a new variation  
    2. Create three different modifiers for the variation
    3. Auto-Generate a child product

### Alternate Exercise: Independent Exercise     
You can try this exercise with your own your own project or business use-case example. As you work through your example, think about the reasoning behind your choices and how they relate to the lessons you've learned.

## **Customer**

This exercise puts into practice creating a customer profile in the dashboard by creating a customer profile:
    1. Add a customer and their contact information
    2. Include two different address entries

## **Flows**

This exercise puts into practice creating a core flow using the dashboard:
    1. Create a core product flow and configure two fields using different field types
    2. Create a core customer flow and configure two fields using different field types

## **Promotions**
Try creating promotions using the dashboard:
    1. Create a % discount promotion with an offer code
    2. Create a $ off discount promotion without an offer code.


## **Settings: Currency**
Challenge yourself by trying these dashboard tasks:
  1. Setup a new currency code and add a price to a product using this new currency.
