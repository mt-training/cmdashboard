## **Playtend Game Company Scenario**

To demonstrate many of the concepts and use-cases scenarios presented throughout this course, we will use a fictitious company called Playtend. For the hands-on exercises, you can apply what you’ve learned by using this sample company. 


#### Playtend Games
Playtend Games, a consumer electronics and video game company, is setting up an online store. The company sells game consoles that can be purchased in various memory sizes, colors, and are compatible with SmartTVs as well as portable players. They also sell online digital games that can be download in multiple formats depending on the device along with gaming accessories such as controllers, headphones, and VR equipment. All products and accessories are marketed under the Playtend brand.

The company has decided to expand its online presence to include two new game consoles (Series S and Z) that will be sold under a new brand called Digiplay. These new consoles have 2TB of storage and are 5x faster than previous series models. The Z console is a portable device that comes in a variety of new colors. The Product Manager has ordered 100 of each unit and wants to sell them with a Playtend game and game controller accessory as he feels this bundle will be a successful marketing opportunity for new gamers.
