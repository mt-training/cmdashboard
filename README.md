# EPCC Commerce Manager Dashboard Exercise Worksheet.
The exercises in this worksheet are designed to apply what you have learned by performing basic hands-on exercises that teach you how to manage the front-end of your web store using EPCC Commerce Manager Dashboard.

## Learning objectives
The following exercises are intended to help you apply what you've learned using the Playtend Gaming example or your own use-case scenario.

1. **Product Catalog and Files**
    1. Create a Catalog
    2. Build a Single Parent Product
    3. Build a Parent Product with Child Products using Variants
2.    **Customers**
3.    **Flows**
4.    **Promotions**
5.    **Settings: Currency**


## Hands-on Exercises

For hands-on exercise visit [Dashboard hands-on Exercises](./EPCC_Dashboard_Exercise.md)

## Game Company Scenario

Read about the fictitious company called [Playtend Games](./Playtend_Game_Company.md)

## Terminology and Best Practices
Review key terms and concepts that will help you understand how to maneuver through the the Commerce Manager dashboard  [EPCC_CM_Terminology_and_Best_Practices](./EPCC_CM_Terminology_and_Best_Practices.md)
